<h2>Настройка RetailRocket</h2>

<form method="POST">
	<fieldset>
    <label for="RRPARTNER_ID">Укажите код счетчика:</label>
	<div class="margin-form">
    <input name="RRPARTNER_ID" id="RRPARTNER_ID" value="{$RRPARTNER_ID}" />
	</div>
	</br>
		</fieldset>
	<div class="clear"></br></div>
	<div class="panel">
	<h3>Персональные рекомендации</h3>
	<div class="row">

	<label for="SHOWPERSONAL">Персональные персональные рекомендации на главной:</label>
	<div class="margin-form">
	<input type="checkbox" name="SHOWPERSONAL" {$SHOWPERSONAL} />
	</div>
	</br>
	<label for="H2PERSONAL">Заголовок над виджетом персональных рекомендаций:</label>
	<div class="margin-form">
    <input name="H2PERSONAL" id="H2PERSONAL" value="{$H2PERSONAL}" />
	</div>
	</br>
	<label for="PERSONALWIDGET">Код виджета персональных рекомендаций:</label>
	<div class="margin-form">
    <textarea name="PERSONALWIDGET">{$PERSONALWIDGET}</textarea>
	</div>
</br>	
</div>
</div>
<div class="clear"></br></div>
	<div class="panel">
	<h3>Общие рекомендации</h3>
	<div class="row">			
	<label for="SHOWCOMMON">Показывать общие рекомендации на главной:</label>
	<div class="margin-form">
	<input type="checkbox" name="SHOWCOMMON" {$SHOWCOMMON} />
	</div>
	</br>
	<label for="H2COMMON">Заголовок над виджетом общих рекомендаций:</label>
	<div class="margin-form">
	</br>
    <input name="H2COMMON" id="H2COMMON" value="{$H2COMMON}" />
	</div>
	</br>
	<label for="COMMONWIDGET">Код виджета общих рекомендаций:</label>
	<div class="margin-form">
    <textarea name="COMMONWIDGET">{$COMMONWIDGET}</textarea>
	</div>

	</br>
</div>
</div>	
<div class="clear"></br></div>
	<div class="panel">
	<h3>Рекомендации в карточке товара</h3>
	<div class="row">	

		<label for="SHOWPRODUCT">Показывать рекомендации в карточке товара:</label>
	<div class="margin-form">
	<input type="checkbox" name="SHOWPRODUCT" {$SHOWPRODUCT} />
	</div>
	</br>
	<label for="H2PRODUCT">Заголовок над виджетом рекомендаций в карточке товара:</label>
	<div class="margin-form">
	</br>
    <input name="H2PRODUCT" id="H2PRODUCT" value="{$H2PRODUCT}" />
	</div>
	</br>
	<label for="PRODUCTWIDGET">Код виджета рекомендаций в карточке товара:</label>
	<div class="margin-form">
    <textarea name="PRODUCTWIDGET">{$PRODUCTWIDGET}</textarea>
	</div>

	</br>
	</div>
</div>	

<div class="clear"></br></div>
	<div class="panel">
	<h3>Рекомендации в корзине</h3>
	<div class="row">	

		<label for="SHOWCART">Показывать рекомендации в корзине:</label>
	<div class="margin-form">
	<input type="checkbox" name="SHOWCART" {$SHOWCART} />
	</div>
	</br>
	<label for="H2CART">Заголовок над виджетом рекомендаций в корзине:</label>
	<div class="margin-form">
	</br>
    <input name="H2CART" id="H2CART" value="{$H2CART}" />
	</div>
	</br>
	<label for="CARTWIDGET">Код виджета рекомендаций в корзине:</label>
	<div class="margin-form">
    <textarea name="CARTWIDGET">{$CARTWIDGET}</textarea>
	</div>

	</br>
	</div>
</div>	

<div class="clear"></br></div>
		<div class="panel">
	<h3>Рекомендации на странице поиска</h3>
	<div class="row">	
	<label for="SHOWSEARCH">Показывать рекомендации в поиске:</label>
	<div class="margin-form">
	<input type="checkbox" name="SHOWSEARCH" {$SHOWSEARCH} />
	</div>
	</br>
	<label for="H2SEARCH">Заголовок над виджетом рекомендаций в поиске:</label>
	<div class="margin-form">
	</br>
    <input name="H2SEARCH" id="H2SEARCH" value="{$H2SEARCH}" />
	</div>
	</br>
	<label for="SEARCHWIDGET">Код виджета рекомендаций в поиске:</label>
	<div class="margin-form">
    <textarea name="SEARCHWIDGET">{$SEARCHWIDGET}</textarea>
	</div>

	</br>
		</div>
</div>	
	<div class="margin-form">
    <input type="submit" value="Сохранить" />
	</div>
</form>

<p>Адрес встроенной YML-выгрузки для экспорта товаров в RetailRocket:</p>

<ul>
    <li>С включенными SEO-адресами: http://{$smarty.server.HTTP_HOST}/module/rr/yml</li>
    <li>С выключенными SEO-адресами: http://{$smarty.server.HTTP_HOST}/index.php?fc=module&module=rr&controller=yml</li>
</ul>