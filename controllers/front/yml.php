<?php


class rrYmlModuleFrontController extends ModuleFrontController
{

    function PrepareString($str)
    {

        $from_char = array('&nbsp;', '"', '&', '>', '<', '\'', '`');
        $to_char = array(' ', '&quot;', '&amp;', '&gt;', '&lt;', '&apos;', '&apos;');
//Удаляем html теги
        $str = preg_replace('!<[^>]*?>!', ' ', $str);
//Преобразуем символы в html сущности
        $str = str_replace($from_char, $to_char, $str);
//Удаляем запрещенные символы
        $str = preg_replace('#[\x00-\x08\x0B-\x0C\x0E-\x1F]+#is', ' ', $str);
//Конвертируем в кодировку windows-1251
        return trim($str);
    }

    function init()
    {

        parent::init();
        $db = DB::getInstance();
        $categories = $db->executeS("SELECT
  cp.id_category,
  c.id_category,
  cl.id_category,
  cl.name AS name,
  c.id_parent

FROM
  " . _DB_PREFIX_ . "category_product AS cp
  LEFT JOIN " . _DB_PREFIX_ . "category AS c
    ON cp.id_category = c.id_category
  LEFT JOIN " . _DB_PREFIX_ . "category_lang AS cl
    ON cl.id_category = c.id_category
  LEFT JOIN " . _DB_PREFIX_ . "product_shop AS ps
    ON ps.id_product = cp.id_product
  LEFT JOIN " . _DB_PREFIX_ . "product AS p
    ON ps.id_product = p.id_product

WHERE ps.`id_shop` = 1 AND ps.`active` = 1 AND ps.`visibility` IN ('both', 'catalog')
GROUP BY cp.id_category;");
        $date = date('Y-m-d H:i');
        $siteurl = $db->getValue('SELECT CONCAT(domain,physical_uri) from ' . _DB_PREFIX_ . 'shop_url');
        $sitename = $db->getValue('SELECT name from ' . _DB_PREFIX_ . 'shop');
        $items = $db->executeS('SELECT
  tt1.id_product                                                              AS id,
  tt2.name,
  tt2.link_rewrite,
  tt1.id_category_default,
  tt1.price,
  tt3.name                                                                    AS manufacturer,
  tt2.description,
  "true" AS available,
  im.id_image

FROM ' . _DB_PREFIX_ . 'product AS tt1
  LEFT JOIN ' . _DB_PREFIX_ . 'product_lang AS tt2
    ON tt1.id_product = tt2.id_product
  LEFT JOIN ' . _DB_PREFIX_ . 'manufacturer AS tt3
    ON tt3.id_manufacturer = tt1.id_manufacturer

LEFT JOIN ' . _DB_PREFIX_ . 'image AS im ON im.id_product=tt1.id_product

WHERE tt1.visibility = "both" AND tt1.price>0 AND tt1.available_for_order=1
GROUP BY tt1.id_product');

        echo '<?xml version="1.0" encoding="utf-8"?>';
        echo '
        <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
    <yml_catalog date="'.$date.'">
        <shop>
        <name>' . $sitename . '</name>
        <company>' . $sitename . '</company>
        <url>http://' . $siteurl . '</url>

        <currencies>
            <currency id="RUR" rate="1" />
        </currencies>

        <categories>';


        foreach ($categories as $category) {
            if (intval($category['id_category']) > 0)
                echo '<category id="' . $category['id_category'] . '"' . ($category['id_parent'] >= 2 ? ' parentId="' . $category['id_parent'] . '"' : ' parentId="0"') . '>' . $this->PrepareString($category['name']) . '</category >' . "\n";
        }
        ?>


        </categories>

        <local_delivery_cost>100</local_delivery_cost>

        <offers>
            <?php
            //TODO: add categories of product. $offerCategories  = explode(',',$offerCategories)
            foreach ($items as $offer) {


                echo '<offer id="' . $offer['id'] . '" available="' . $offer['available'] . '">' .
                    '<url>'.$this->context->link->getProductLink($offer['id']).'</url>' . "\n" .
                    ' <price>' . $offer['price'] . '</price>' . "\n" .
                    '<currencyId>RUR</currencyId>' . "\n" .
                    '<categoryId>' . $offer['id_category_default'] . '</categoryId>' . "\n" .
                    '<picture>';

                $image_id = $offer['id_image'];
                echo "http://" . $siteurl . "img/p/" . implode("/", str_split($image_id)) . "/" . $image_id . ".jpg";

                echo '</picture>' .
                    ' <delivery>true</delivery>';


                echo '<name>' . $this->PrepareString($offer['name']) . '</name>' . "\n";
                echo '<vendor>' . $this->PrepareString($offer['manufacturer']) . '</vendor>';
                echo "\n";

                echo "\n";
                echo '<description>' . $this->PrepareString($offer['description']) . '</description>' . "\n";
                echo '</offer>';
            }

            echo '</offers></shop></yml_catalog>';
            die();
    }
}