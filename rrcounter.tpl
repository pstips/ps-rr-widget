<script>
    var rrPartnerId = "{$RRPARTNER_ID}";
    {literal}
    var rrApi = {};
    var rrApiOnReady = [];
    rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
            rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
    (function(d) {
        var ref = d.getElementsByTagName('script')[0];
        var apiJs, apiJsId = 'rrApi-jssdk';
        if (d.getElementById(apiJsId)) return;
        apiJs = d.createElement('script');
        apiJs.id = apiJsId;
        apiJs.async = true;
        apiJs.src = "//cdn.retailrocket.ru/javascript/api.js";
        ref.parentNode.insertBefore(apiJs, ref);
    }(document));{/literal}</script>