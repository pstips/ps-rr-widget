<?php
if (!defined('_PS_VERSION_'))
    exit;

class rr extends Module
{
    private $_html = '';
    private $_postErrors = array();
    private $hooks = array('displayFooter', 'displayOrderConfirmation', 'extraLeft', 'displayShoppingCart', 'displayHeader', 'displayHomeTabContent', 'displayFooterProduct');

    function __construct()
    {

        $this->name = 'rr';
        $this->tab = 'export';
        $this->version = '2';
        $this->author = 'Konstatin Pavlovsky Prestashoptips.ru';
        $this->need_instance = 0;
        $this->module_key = '';

        parent::__construct();

        $this->displayName = $this->l('Retail Rocket Intergration');
        $this->description = $this->l('Product recommendations by RR');
    }

    public function registerHooks()
    {

        foreach ($this->hooks as $hook) {
            if (!$this->registerHook($hook)) {
                $this->_errors[] = "Failed to install hook '$hook'<br />\n";
                return false;
            }
        }

        return true;
    }

    public function unregisterHooks()
    {

        foreach ($this->hooks as $hook) {
            if (!$this->unregisterHook($hook)) {
                $this->_errors[] = "Failed to uninstall hook '$hook'<br />\n";
                return false;
            }
        }

        return true;
    }

    public function install()
    {
        return (parent::install()
            && Configuration::updateValue('RRPARTNER_ID', '')
			&& Configuration::updateValue('SHOWPERSONAL', '')
			&& Configuration::updateValue('H2PERSONAL', '')
			&& Configuration::updateValue('PERSONALWIDGET', '')
			&& Configuration::updateValue('SHOWCOMMON', '')
			&& Configuration::updateValue('H2COMMON', '')
			&& Configuration::updateValue('COMMONWIDGET', '')
			&& Configuration::updateValue('SHOWPRODUCT', '')
			&& Configuration::updateValue('H2PRODUCT', '')
			&& Configuration::updateValue('PRODUCTWIDGET', '')
			&& Configuration::updateValue('SHOWCART', '')
			&& Configuration::updateValue('H2CART', '')
			&& Configuration::updateValue('CARTWIDGET', '')
			&& Configuration::updateValue('SHOWSEARCH', '')
			&& Configuration::updateValue('H2SEARCH', '')
			&& Configuration::updateValue('SEARCHWIDGET', '')
            && $this->registerHooks()
        );
    }

    public function uninstall()
    {
        return (parent::uninstall()
            && Configuration::deleteByName('RRPARTNER_ID')
            && $this->unregisterHooks()
        );
    }

    public function getContent()
    {
		
		if ($_SERVER['REQUEST_METHOD'] =='POST'){
        if (array_key_exists('RRPARTNER_ID', $_POST)) {
        Configuration::updateValue('RRPARTNER_ID', $_POST['RRPARTNER_ID']);
        }
		
		if (array_key_exists('SHOWPERSONAL', $_POST)) {
        Configuration::updateValue('SHOWPERSONAL', 'checked');
        }else{
		Configuration::updateValue('SHOWPERSONAL', '');
		}
		
		if (array_key_exists('H2PERSONAL', $_POST)) {
        Configuration::updateValue('H2PERSONAL', $_POST['H2PERSONAL']);
        }

		if (array_key_exists('PERSONALWIDGET', $_POST)) {
        Configuration::updateValue('PERSONALWIDGET', htmlspecialchars($_POST['PERSONALWIDGET']));
		//htmlspecialchars нужен, потому что без него условный оператор в хуке не может определиться есть что-то в этой переменной или нет
        }

		if (array_key_exists('SHOWCOMMON', $_POST)) {
        Configuration::updateValue('SHOWCOMMON', 'checked');
        }else{
		Configuration::updateValue('SHOWCOMMON', '');
		}
		
		if (array_key_exists('H2COMMON', $_POST)) {
        Configuration::updateValue('H2COMMON', $_POST['H2COMMON']);
        }

		if (array_key_exists('COMMONWIDGET', $_POST)) {
        Configuration::updateValue('COMMONWIDGET', htmlspecialchars($_POST['COMMONWIDGET']));
        }
		
		if (array_key_exists('SHOWPRODUCT', $_POST)) {
        Configuration::updateValue('SHOWPRODUCT', 'checked');
        }else{
		Configuration::updateValue('SHOWPRODUCT', '');
		}
		
		if (array_key_exists('H2PRODUCT', $_POST)) {
        Configuration::updateValue('H2PRODUCT', $_POST['H2PRODUCT']);
        }

		if (array_key_exists('PRODUCTWIDGET', $_POST)) {
        Configuration::updateValue('PRODUCTWIDGET', htmlspecialchars($_POST['PRODUCTWIDGET']));
        }
		
		if (array_key_exists('SHOWCART', $_POST)) {
        Configuration::updateValue('SHOWCART', 'checked');
        }else{
		Configuration::updateValue('SHOWCART', '');
		}
		
		if (array_key_exists('H2CART', $_POST)) {
        Configuration::updateValue('H2CART', $_POST['H2CART']);
        }

		if (array_key_exists('CARTWIDGET', $_POST)) {
        Configuration::updateValue('CARTWIDGET', htmlspecialchars($_POST['CARTWIDGET']));
        }
		
		if (array_key_exists('SHOWSEARCH', $_POST)) {
        Configuration::updateValue('SHOWSEARCH', 'checked');
        }else{
		Configuration::updateValue('SHOWSEARCH', '');
		}
		
		if (array_key_exists('H2SEARCH', $_POST)) {
        Configuration::updateValue('H2SEARCH', $_POST['H2SEARCH']);
        }

		if (array_key_exists('SEARCHWIDGET', $_POST)) {
        Configuration::updateValue('SEARCHWIDGET', htmlspecialchars($_POST['SEARCHWIDGET']));
        }
		
		}

        $this->context->smarty->assign(
		array(
		'RRPARTNER_ID'=> Configuration::get('RRPARTNER_ID'),
		'SHOWPERSONAL'=> Configuration::get('SHOWPERSONAL'),
		'H2PERSONAL'=> Configuration::get('H2PERSONAL'),
		'PERSONALWIDGET'=> Configuration::get('PERSONALWIDGET'),
		'SHOWCOMMON'=> Configuration::get('SHOWCOMMON'),
		'H2COMMON'=> Configuration::get('H2COMMON'),
		'COMMONWIDGET'=> Configuration::get('COMMONWIDGET'),
		'SHOWCART'=> Configuration::get('SHOWCART'),
		'H2CART'=> Configuration::get('H2CART'),
		'CARTWIDGET'=> Configuration::get('CARTWIDGET'),
		'SHOWPRODUCT'=> Configuration::get('SHOWPRODUCT'),
		'H2PRODUCT'=> Configuration::get('H2PRODUCT'),
		'PRODUCTWIDGET'=> Configuration::get('PRODUCTWIDGET'),
		'SHOWSEARCH'=> Configuration::get('SHOWSEARCH'),
		'H2SEARCH'=> Configuration::get('H2SEARCH'),
		'SEARCHWIDGET'=> Configuration::get('SEARCHWIDGET')
		)
		);
        return $this->display(__FILE__, 'adminform.tpl');
    }

    function hookDisplayHeader($params)
    {
	
	        switch (get_class($this->context->controller)) {
				case 'CategoryController':
                //display tracking code for category page
                $this->context->smarty->assign('rr_category', '1');
                break;

				case 'ProductController':
                //display tracking code for product page
                $this->context->smarty->assign('rr_product', '1');
                break;
        
				case 'SearchController':
				if((Configuration::get('SHOWSEARCH'))&(Configuration::get('H2SEARCH'))&(Configuration::get('SEARCHWIDGET'))){
				
					$this->context->smarty->assign(array(
					'search_title'=> Configuration::get('H2SEARCH'),
					'search_widget'=> str_replace('<ключевое слово>', Tools::getValue('search_query'), htmlspecialchars_decode(Configuration::get('SEARCHWIDGET')))
				));	
				}
				break;	
			}
	
	}
	
    function hookExtraLeft($params)
    {

    }

    public function hookDisplayFooter($params)
    {
        $this->context->smarty->assign('RRPARTNER_ID', Configuration::get('RRPARTNER_ID'));
        return $this->display(__FILE__, 'api.tpl');
    }


    public function hookDisplayOrderConfirmation($params)
    {
        $order = $params['objOrder'];
        $params = array();
        $params['userId'] = $order->id_customer;
        $params['transaction'] = (int)($order->id);

        $products = $order->getProducts();
        $productsOut = array();
        foreach ($products as $product) {
            $productsOut[] = array('id' => $product['id_product'], 'price' => $product['price'], "qnt" => (int)($product['product_quantity']));
        }
        $params['items'] = $productsOut;
        $orderParams = json_encode($params);
        return '<script type="text/javascript"> function rcAsyncInit() {rrApi.order(' . $orderParams . ');}</script>';
    }
	
    public function hookDisplayHomeTabContent($params) {
	
		if((Configuration::get('SHOWPERSONAL'))&(Configuration::get('H2PERSONAL'))&(Configuration::get('PERSONALWIDGET')))
			$showp='<h2>'.Configuration::get('H2PERSONAL').'</h2>'.htmlspecialchars_decode(Configuration::get('PERSONALWIDGET'));
	
	
		if((Configuration::get('SHOWCOMMON'))&(Configuration::get('H2COMMON'))&(Configuration::get('COMMONWIDGET')))
			$showc='<h2>'.Configuration::get('H2COMMON').'</h2>'.htmlspecialchars_decode(Configuration::get('COMMONWIDGET'));
	
	
	return $showp.$showc;
	}
    
	public function hookDisplayShoppingCart ($params){
		if((Configuration::get('SHOWCART'))&(Configuration::get('H2CART'))&(Configuration::get('CARTWIDGET'))){

		$products=$this->context->cart->getProducts();	
		if(isset($products)){
		$length=count($products);
				foreach($products as $product){
		$id_prdouct=$id_prdouct.$product['id_product'];
		if($length-1>$i){
		$id_prdouct=$id_prdouct.', ';
		$i++;
		}
		}
		}

		
			return'<h2>'.Configuration::get('H2CART').'</h2>'.str_replace('<products_id>', $id_prdouct, htmlspecialchars_decode(Configuration::get('CARTWIDGET')));}		
	}
		
    public function hookDisplayFooterProduct($params){
	

		if((Configuration::get('SHOWPRODUCT'))&(Configuration::get('H2PRODUCT'))&(Configuration::get('PRODUCTWIDGET')))
		return'<h2>'.Configuration::get('H2PRODUCT').'</h2>'. str_replace('<product_id>', intval(Tools::getValue('id_product')), htmlspecialchars_decode(Configuration::get('PRODUCTWIDGET')));
		
		
	 
    
	}
	
}